#!/bin/bash

if [ "$PGUSER" != "esignforms" ]; then
  echo $0 - $(date) - "ERROR: Please export/set the PGUSER and PGPASSWORD for 'esignforms' OpenESF user DBA admin
."
  exit 1
fi

DEBUGOPTIONS=
if [ $# -eq 1 ]; then
  CONTEXTPATH=$1
elif [ $# -eq 2 ]; then
  CONTEXTPATH=$1
  DEBUGOPTIONS=y
else
  echo -n "Please enter the webapp's context path (i.e. 'demo' or 'ROOT'): "
  read CONTEXTPATH
  echo -n "Use debugger options? (y/n) "
  read ANS
  if [ "y" = "$ANS" ]; then
    DEBUGOPTIONS=y
  fi
fi

if [ -z "$WESF_DEPLOYMENT_BASE_TABLESPACE" ]; then
   JAVAOPTIONS="-server -Xms32m"
   WESF_DEPLOYMENT_BASE="$ESF_DEPLOYMENT_BASE"
else
   JAVAOPTIONS=
fi

. ~/bin/setclasspath $CONTEXTPATH

if [ "$DEBUGOPTIONS" = "y" ]; then
  if [ -z "$WESF_DEPLOYMENT_BASE_TABLESPACE" ]; then
     DEBUGOPTIONS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n"
  else
     DEBUGOPTIONS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n"
  fi
fi

cd "$CATALINA_HOME/webapps"
cd "$CONTEXTPATH"

echo $0 - $(date) - Starting DbSetup at $(pwd)
echo java $JAVAOPTIONS $DEBUGOPTIONS -Dcatalina.base="$CATALINA_HOME" -Desf.deploybase="$WESF_DEPLOYMENT_BASE" com.esignforms.open.db.tools.DbSetup /$CONTEXTPATH
java $JAVAOPTIONS $DEBUGOPTIONS -Dcatalina.base="$CATALINA_HOME" -Desf.deploybase="$WESF_DEPLOYMENT_BASE" com.esignforms.open.db.tools.DbSetup /$CONTEXTPATH
echo $0 - $(date) - DbSetup stopped
